#! python3

# This file is part of vial-qmk-firmwares
# Copyright (C) 2023  Niko Wenselowski

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from json import load
from operator import itemgetter
from os import getenv

from jinja2 import Environment, FileSystemLoader, select_autoescape

env = Environment(loader=FileSystemLoader("templates"), autoescape=select_autoescape())


def main():
    keyboards = load_keyboards("keyboards.json")
    print(f"Loaded data of {len(keyboards)} keyboards")
    html = create_html(keyboards)
    write_file(html, "index.html")


def load_keyboards(filename: str):
    with open(filename) as keebfile:
        return load(keebfile)


def create_html(keyboards):
    qmk_commit_ref = getenv("QMK_REF")
    commit_repo = getenv("QMK_REPO")
    if commit_repo.endswith(".git"):
        commit_repo = commit_repo[:-4]
    commit_address = f"{commit_repo}/commit/{qmk_commit_ref}"
    render_time = datetime.now()

    template = env.get_template("index.html.j2")

    sorted_keyboards = sorted(keyboards, key=itemgetter("identifier"))

    rendered = template.render(
        keyboards=sorted_keyboards,
        render_time=render_time,
        commit_address=commit_address,
        commit_ref=qmk_commit_ref,
    )
    return rendered


def write_file(html: str, filename: str):
    with open(filename, "w") as htmlfile:
        htmlfile.write(html)


if __name__ == "__main__":
    main()
