#! python3

# This file is part of vial-qmk-firmwares
# Copyright (C) 2023  Niko Wenselowski

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from json import dump, dumps
from os import getenv, sep
from pathlib import Path

KEYBOARDS = getenv("KEYBOARD", "").split()
KEYMAP = getenv("KEYMAP", "vial")


def main():
    args = parse_cli()
    searchpath = Path(args.folder).absolute()
    keebs = get_keyboards(searchpath)

    if not keebs:
        raise RuntimeError("Found no configs")

    if args.json:
        print(f"Found {len(keebs)} keyboards")
        write_results(keebs, args.json)
    else:
        print(dumps(keebs))


def parse_cli():
    parser = ArgumentParser()
    parser.add_argument(
        "--folder",
        default="keyboards",
        help="Folder with keyboard definitions, i.e. qmk/keyboards",
    )
    parser.add_argument(
        "--json", default="keyboards.json", help="Writes outputs to the given file"
    )

    return parser.parse_args()


def get_keyboards(keyboards_folder_path: str):
    def recurse_folders(folder):
        for item in folder.iterdir():
            if item.is_dir():
                yield from recurse_folders(item)
            elif item.is_file():
                yield item

    if KEYBOARDS:
        print(f"Looking for {', '.join(KEYBOARDS)}")
    print(f"Searching for keymap {KEYMAP}")

    if not keyboards_folder_path.is_dir():
        raise RuntimeError(f"Expected {keyboards_folder_path} to be a folder")

    base_len = len(str(keyboards_folder_path.absolute()))
    keyboards = []
    for element in recurse_folders(keyboards_folder_path):
        element_path = str(element.resolve())

        if "vial_example" in element_path:
            continue

        if KEYBOARDS:
            if not any(keyboard in element_path for keyboard in KEYBOARDS):
                continue

        if element.match(f'*/{KEYMAP}/rules.mk'):
            path_without_base = element_path[base_len:]
            keeb = parse_path(path_without_base)
            keeb["settings"] = parse_rules(element_path)
            keyboards.append(keeb)

    return keyboards


def parse_path(path):
    # There are different ways for a path:
    # - keyboards / <model> / keymaps / vial / filename
    # - keyboards / <manu> / <model> / keymaps / vial / filename
    # - keyboards / <manu> / <model> / <variant> / keymaps / vial / filename
    # - keyboards / <manu> / <model> / <variant> / <version> / keymaps / vial / filename
    # - keyboards / <manu> / <model> / <variant> / <mcu> / <revision> / keymaps / vial / filename

    separators = path.count(sep)

    if separators == 4:
        _, model, _, keymap, _ = path.split(sep)
        keeb = {"model": model, "keymap": keymap}
    elif separators == 5:
        _, manu, model, _, keymap, _ = path.split(sep)
        keeb = {"manufacturer": manu, "model": model, "keymap": keymap}
    elif separators == 6:
        _, manu, model, variant, _, keymap, _ = path.split(sep)
        keeb = {
            "manufacturer": manu,
            "model": model,
            "variant": variant,
            "keymap": keymap,
        }
    elif separators == 7:
        _, manu, model, variant, version, _, keymap, _ = path.split(sep)
        keeb = {
            "manufacturer": manu,
            "model": model,
            "variant": variant,
            "version": version,
            "keymap": keymap,
        }

    elif separators == 8:
        _, manu, model, variant, mcu, version, _, keymap, _ = path.split(sep)
        keeb = {
            "manufacturer": manu,
            "model": model,
            "variant": variant,
            "mcu": mcu,
            "version": version,
            "keymap": keymap,
        }
    else:
        raise RuntimeError(
            f"Failed to parse: {path} ({separators} separators)"
        )            

    return keeb


def parse_rules(filepath):
    def contains_unsupported_tokens(line: str) -> bool:
        # Conditions would require more parsing logic -> ignore
        if line.startswith(("ifeq", "ifneq", "ifndef", "endif", "else")):
            return True

        # Expanding would require more parsing logic -> ignore
        if "+=" in line:
            return True

        return False
        
    settings = {}
    with open(filepath) as f:
        for line in f:
            line = line.strip()

            if not line:  # empty line
                continue

            if line.startswith("#"):
                continue

            if contains_unsupported_tokens(line):
                continue

            try:
                key, value = line.split("=", 1)
            except ValueError:
                raise RuntimeError(f"Failed to parse {filepath!r} line: {line!r}")

            try:
                value, _ = value.split("#", 1)
            except ValueError:
                pass

            settings[key.strip()] = value.strip()

    return settings


def write_results(results: list, outputfile: str):
    with open(outputfile, "w") as resultfile:
        dump(results, resultfile)


if __name__ == "__main__":
    main()
