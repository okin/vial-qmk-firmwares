#! python3

# This file is part of vial-qmk-firmwares
# Copyright (C) 2023  Niko Wenselowski

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from itertools import islice
from json import dump, load
from math import ceil


def main():
    args = parse_cli()
    input_json = load_json(args.json)
    if not input_json:
        raise RuntimeError(f"Found no data in {args.json}")

    lists = split_list(input_json, args.parts)

    for index, list_element in enumerate(lists, start=1):
        write_results(list_element, f"split_keyboards-{index}.json")


def parse_cli():
    parser = ArgumentParser()
    parser.add_argument(
        "--json", default="keyboards.json", help="Read from the given file"
    )
    parser.add_argument(
        "--parts", default=5, type=int, help="Amount of parts to create"
    )

    return parser.parse_args()


def load_json(filename: str):
    with open(filename) as f:
        return load(f)


def split_list(input_list: list, amount: int = 5) -> list:
    batch_amount = ceil(len(input_list) / amount)
    lists = batched(input_list, batch_amount)
    lists = [list(partial_list) for partial_list in lists]
    return lists


# Taken from https://docs.python.org/3/library/itertools.html#itertools-recipes
def batched(iterable, n):
    "Batch data into tuples of length n. The last batch may be shorter."
    # batched('ABCDEFG', 3) --> ABC DEF G
    if n < 1:
        raise ValueError("n must be at least one")
    it = iter(iterable)
    while batch := tuple(islice(it, n)):
        yield batch


def write_results(results: list, outputfile: str):
    with open(outputfile, "w") as resultfile:
        dump(results, resultfile)


if __name__ == "__main__":
    main()
