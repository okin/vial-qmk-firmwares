#! python3

# This file is part of vial-qmk-firmwares
# Copyright (C) 2022  Niko Wenselowski

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from json import load, dump
from os import getenv
from pathlib import Path
from random import choices
import re
from shutil import move
from subprocess import PIPE, STDOUT, check_output, run, CalledProcessError

RANDOM_ELEMENTS = 5


def main():
    args = parse_cli()
    keebs = load_keyboards(args.keyboard_file)
    keebs = filter_keebs(keebs)
    results = build_firmwares(keebs, args.binary_folder)

    if args.output_file:
        write_results(results, args.output_file)


def parse_cli():
    parser = ArgumentParser()
    parser.add_argument(
        "--binary-folder",
        dest="binary_folder",
        required=True,
        help="Folder where firmware binaries should be collected",
    )
    parser.add_argument(
        "--input-file",
        dest="keyboard_file",
        help="File to parse for keyboard/keymap information",
    )
    parser.add_argument(
        "--output-file", dest="output_file", help="File to write results to."
    )

    return parser.parse_args()


def load_keyboards(filename: str):
    with open(filename) as keebfile:
        return load(keebfile)


def filter_keebs(keebs: list):
    # temp fix for having a working build.
    # see https://gitlab.com/okin/vial-qmk-firmwares/-/issues/17 for more info
    keebs = [k for k in keebs if k["model"] != "neodox"]

    if not is_running_in_ci():
        return keebs

    def make_items_unique(items: list) -> list:
        unique_items = []
        for item in items:
            if item not in unique_items:
                unique_items.append(item)

        return unique_items

    print(f"CI detected. Limiting to {RANDOM_ELEMENTS} builds")
    selected_boards = choices(keebs, k=RANDOM_ELEMENTS)
    return make_items_unique(selected_boards)


def is_running_in_ci():
    current_branch = getenv("CI_COMMIT_BRANCH")
    default_branch = getenv("CI_DEFAULT_BRANCH")

    if not (current_branch and default_branch):
        return False

    if current_branch == default_branch:
        # We are running in the default branch and want to build all
        return False

    # We are running in a CI branch
    return True


def build_firmwares(keyboards: list, binary_folder: str):
    def get_checksums(filepath):
        def get_checksum(program, filepath):
            output = check_output([program, filepath])
            output = output.decode()
            hashsum, *_ = output.split(" ")
            return hashsum

        return {
            "md5": get_checksum("md5sum", filepath),
            "sha256": get_checksum("sha256sum", filepath),
        }

    binary_folder = Path(binary_folder)

    build_results = []
    for index, keeb in enumerate(keyboards, start=1):
        board_identifier = create_board_identifier(keeb)
        keeb["identifier"] = board_identifier

        run(["qmk", "clean"], capture_output=True, check=True)

        print(f"Building firmware for #{index}: {board_identifier}")
        command = get_build_command(keeb)

        process_result = run(command, text=True, stdout=PIPE, stderr=STDOUT)

        try:
            process_result.check_returncode()
            print(f"Done building firmware for {board_identifier}")

            for line in process_result.stdout.split("\n"):
                if line.lower().startswith("copying") and "to qmk_firmware folder" in line:
                    filepath = line.split("opying ")[1]
                    firmware_filename = filepath.split()[0]

                    checksums = get_checksums(firmware_filename)

                    move(firmware_filename, binary_folder)
                    firmware_path = binary_folder / firmware_filename

                    # Remove leading .. from dir
                    while firmware_path.parts[0] == '..':
                        firmware_path = Path(*firmware_path.parts[1:])

                    break
            else:
                print(process_result.stdout)
                raise RuntimeError("Unable to find path to file for flashing")

            keeb["build"] = {
                "success": True,
                "firmware_filename": firmware_filename,
                "firmware_path": str(firmware_path),
                "logs": process_result.stdout.strip().split("\n"),
                "checksums": checksums,
            }
        except CalledProcessError as process_error:
            print(f"Failed to build firmware for {board_identifier}: {process_error}")
            keeb["build"] = {
                "success": False,
                "firmware_filename": None,
                "firmware_path": None,
                "logs": process_result.stdout.split("\n") or [],
            }

        keeb["build"]["logs"] = clean_logs(keeb["build"]["logs"])
        build_results.append(keeb)

    try:
        print(f"{index} builds made")
    except NameError:
        print("WARNING: No builds made!")

    return build_results


def create_board_identifier(keyboard, seperator="_"):
    return seperator.join(get_path_thingy(keyboard))


def get_path_thingy(keyboard):
    try:
        yield keyboard["manufacturer"]
    except KeyError:
        pass

    yield keyboard["model"]

    try:
        yield keyboard["variant"]
    except KeyError:
        pass

    try:
        yield keyboard["mcu"]
    except KeyError:
        pass

    try:
        yield keyboard["version"]
    except KeyError:
        pass


def get_build_command(keyboard):
    model = keyboard["model"]
    keymap = keyboard["keymap"]

    try:
        manu = keyboard["manufacturer"]
        board_ref = f"{manu}/{model}"
    except KeyError:
        board_ref = model

    try:
        mcu = keyboard["mcu"]
        board_ref = f"{board_ref}/{mcu}"
    except KeyError:
        pass

    try:
        variant = keyboard["variant"]
        board_ref = f"{board_ref}/{variant}"
    except KeyError:
        pass
    
    board_ref = create_board_identifier(keyboard, seperator="/")

    return [
        "qmk",
        "compile",
        "--keyboard",
        board_ref,
        "--keymap",  # has to be after board
        keymap,
    ]


def clean_logs(log_lines: list):
    return [
        re.sub(r'\[[0-9;]+m', '', line)
        for line in log_lines
    ]


def write_results(results: dict, filename: str):
    with open(filename, "w") as resultsfile:
        dump(results, resultsfile)


if __name__ == "__main__":
    main()
