#! python3

# This file is part of vial-qmk-firmwares
# Copyright (C) 2023  Niko Wenselowski

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from json import dump, load


def main():
    args = parse_cli()
    combined_json = load_jsons(args.inputs)
    write_results(combined_json, args.json)


def parse_cli():
    parser = ArgumentParser()
    parser.add_argument(
        "--inputs", nargs="+", required=True, help="Read from the given file"
    )
    parser.add_argument(
        "--json", default="keyboards.json", help="Write to the given file"
    )

    return parser.parse_args()


def load_jsons(filenames: list[str]) -> list:
    result_list = []

    for filename in filenames:
        with open(filename) as f:
            json_from_file = load(f)

        result_list.extend(json_from_file)

    return result_list


def write_results(results: list, outputfile: str):
    with open(outputfile, "w") as resultfile:
        dump(results, resultfile)


if __name__ == "__main__":
    main()
